<?php if (!empty($prev_url)) : ?>
    <span class="date-prev">
        <?php
            $text = '←';
            print l(t($text), $prev_url, $prev_options);
        ?>
    </span>
<?php endif; ?>
<h3>
    <?php print $nav_title; ?>
</h3>

<?php if (!empty($next_url)) : ?>
    <span class="date-next">
        <?php print l(($mini ? '' : t('→', array(), array('context' => 'date_nav')) . ' '), $next_url, $next_options); ?>
    </span>
<?php endif; ?>