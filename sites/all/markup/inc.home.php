<section class="front-slider">
    <div id="slider1_container" style="position: relative; top: 0px; left: 0px; 
        height: 600px; overflow: hidden;">
        <div u="slides" class="slider1_container" style="cursor: move; position: absolute; left: 0px; top: 0px; right:0;
            margin: auto; width: 1984px; height: 600px;
            overflow: hidden;">
            <div class="slide-item"  style="background:url('images/slide1.jpg');">
                <div class="slide-info">
                    <a href="#" class="slider-readmore upper-font">გაიგეთ მეტი</a>
                    <div class="slide-info-overflow">
                        <h2 class="slider-title upper-font">ხრაშუნა პურიხრაშუნა </h2>
                        <p> ესტუმრეთ სპარის ნებისმიერ ფილიალს
                            და გასინჯეთ ახლად გამომცხვარი პურ-
                            ფუნთუშეულისტუმრეთ სპარის ნებისმიერ ფილიალს
                            და გასინჯე
                            ფუნთუშეული.
                        </p>
                    </div>
                </div>
            </div>
            <div class="slide-item"  style="background:url('images/slide1.jpg');">
                <div class="slide-info">
                    <a href="#" class="slider-readmore upper-font">გაიგეთ მეტი</a>
                    <div class="slide-info-overflow">
                        <h2 class="slider-title upper-font">ხრაშუნა პურიხრაშუნა </h2>
                        <p> ესტუმრეთ ს
                            ფუნთუშეულისტუმრეთ სპარის ნებისმიერ ფილიალს.
                        </p>
                    </div>
                </div>
            </div>
            <div class="slide-item"  style="background:url('images/slide1.jpg');">
                <div class="slide-info">
                    <a href="#" class="slider-readmore upper-font">გაიგეთ მეტი</a>
                    <div class="slide-info-overflow">
                        <h2 class="slider-title upper-font">ხრაშუნა პურიხრაშუნა </h2>
                        <p> ესტუმრეთ სპარის ნებისმიერ ფილიალს
                            და გასინჯეთ ახლად გამომცხვარი პურ-
                            ფუნთუშეული.
                        </p>
                    </div>
                </div>
            </div>
            <div class="slide-item"  style="background:url('images/slide1.jpg');">
                <div class="slide-info">
                    <a href="#" class="slider-readmore upper-font">გაიგეთ მეტი</a>
                    <div class="slide-info-overflow">
                        <h2 class="slider-title upper-font">ხრაშუნა პურიხრაშუნა </h2>
                        <p> ესტუმრეთ სპარის ნებისმიერ ფილიალს
                            და გასინჯეთ ახლად გამომცხვარი პურ-
                            ფუნთუშეულისტუმრეთ სპარის ნებისმიერ ფილიალს
                            ფუნთუშეული.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div data-u="navigator" class="jssorb01" style="bottom:16px;right:16px;" data-autocenter="1">
            <div data-u="prototype" style="width:12px;height:12px;"></div>
        </div>
        <!-- Arrow Left -->
        <div class="container front-slider-arrows">
            <span data-u="arrowleft" class="jssora05l front-slider-arrow-left" style="top:0px;left:8px;width:40px;height:40px;" data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora05r front-slider-arrow-right" style="top:0px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
        </div>
    </div>
    <!-- Jssor Slider End -->
</section>
<section class="current-offers section-style" >
    <div class="container">
        <div class="row">
            <h3 class="section-title upper-font col-md-12"> მიმდინარე აქციები</h3>
            <div class="clearfix">
                <div class="col-md-4 current-offers-item">
                    <div class="current-offers-img">
                        <a href="#"> <img src="images/current-offers-img.jpg" alt="" />
                        <span class="discount-price"> <span class="old-price">12.95¢ </span>  <span class="new-price"> 9.95¢</span></span></a>
                    </div>
                    <span class="current-offer-action upper-font">აქცია</span>
                    <h2 class="current-offer-title upper-font"><a href="#">თელიანი ველის ცქრიალა ღვინო საახალწლო ფასად!</a></h2>
                </div>
                <div class="col-md-4 current-offers-item">
                    <div class="current-offers-img">
                        <a href="#"> <img src="images/current-offers-img.jpg" alt="" />
                        <span class="discount-price"> <span class="old-price">12.95¢ </span>  <span class="new-price"> 9.95¢</span></span></a>
                    </div>
                    <span class="current-offer-action upper-font">აქცია</span>
                    <h2 class="current-offer-title upper-font"><a href="#">თელიანი ველის ცქრიალა ღვინო საახალწლო ფასად!</a></h2>
                </div>
                <div class="col-md-4 current-offers-item">
                    <div class="current-offers-img">
                        <a href="#"> <img src="images/current-offers-img.jpg" alt="" />
                        <span class="discount-price"> <span class="old-price">12.95¢ </span>  <span class="new-price"> 9.95¢</span></span></a>
                    </div>
                    <span class="current-offer-action upper-font">აქცია</span>
                    <h2 class="current-offer-title upper-font"><a href="#">თელიანი ველის ცქრიალა ღვინო საახალწლო ფასად!</a></h2>
                </div>
            </div>
            <div class="current-more-offers">
                <a href="#" class="moer-offers upper-font">სხვა აქციები და კომბო-შეთავაზებები</a>
            </div>
        </div>
    </div>
</section>
<section class="our-products section-style" >
    <div class="container">
        <div class="row">
            <h3 class="section-title upper-font col-md-12"> ჩვენი პროდუქცია</h3>
            <div class="tabset0">
                <!-- One Tab  -->
                <div data-pws-tab="tab1" data-pws-tab-name='<span class="our-products-tab veget">ხილ-ბოსტნეული </span>' >
                    <!-- Slider  -->
                    <div class="our-products-slider-block">
                        <div id="our-products-slider" class=" clearfix">
                            <div class="our-products-items our-products-slider">
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider  -->
                </div>
                <!-- One Tab  -->       
                <!-- One Tab  -->
                <div data-pws-tab="tab2" data-pws-tab-name='<span class="our-products-tab halfpabric">ნახევარფაბრიკატი</span>' >
                    <!-- Slider  -->
                    <div class="our-products-slider-block">
                        <div id="our-products-slider" class=" clearfix">
                            <div class="our-products-items our-products-slider">
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider  -->
                </div>
                <!-- One Tab  -->        
                <!-- One Tab  -->
                <div data-pws-tab="tab3" data-pws-tab-name='<span class="our-products-tab cakes">საკონდიტრო</span>' >
                    <!-- Slider  -->
                    <div class="our-products-slider-block">
                        <div id="our-products-slider" class=" clearfix">
                            <div class="our-products-items our-products-slider">
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider  -->
                </div>
                <!-- One Tab  -->        
                <!-- One Tab  -->
                <div data-pws-tab="tab4" data-pws-tab-name='<span class="our-products-tab drink">სასმელები</span>' >
                    <!-- Slider  -->
                    <div class="our-products-slider-block">
                        <div id="our-products-slider" class=" clearfix">
                            <div class="our-products-items our-products-slider">
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider  -->
                </div>
                <!-- One Tab  -->          
                <!-- One Tab  -->
                <div data-pws-tab="tab5" data-pws-tab-name='<span class="our-products-tab gastro">გასტრონომია</span>' >
                    <!-- Slider  -->
                    <div class="our-products-slider-block">
                        <div id="our-products-slider" class=" clearfix">
                            <div class="our-products-items our-products-slider">
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider  -->
                </div>
                <!-- One Tab  -->   
                <!-- One Tab  -->
                <div data-pws-tab="tab6" data-pws-tab-name='<span class="our-products-tab all-cat">ყველა კატეგორია</span>' >
                    <!-- Slider  -->
                    <div class="our-products-slider-block">
                        <div id="our-products-slider" class=" clearfix">
                            <div class="our-products-items our-products-slider">
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                                <div class="our-products-item">
                                    <a href="#">
                                        <img src="images/our-products-img.jpg" alt="" />
                                        <span class="our-products-desc">
                                            <h2 class="">ჭოპორტის მანგო</h2>
                                            <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                        </span>
                                    </a>
                                    <a class="our-products-readmore upper-font">დეტალურად</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider  -->
                </div>
                <!-- One Tab  -->  
            </div>
        </div>
    </div>
</section>
<section class="front-news section-style" >
    <div class="container">
        <div class="row">
            <h3 class="section-title upper-font col-md-12"> ახალი ამბები</h3>
            <div class="clearfix">
                <div class="col-md-4 front-news-item">
                    <div class="front-news-img">
                        <a href="#"> <img src="images/front-news-img.jpg" alt="" />
                        </a>
                    </div>
                    <div class="front-news-date upper-font">28 დეკ. 2015</div>
                    <h2><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                    <p>გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                </div>
                <div class="col-md-4 front-news-item">
                    <div class="front-news-img">
                        <a href="#"> <img src="images/front-news-img.jpg" alt="" />
                        </a>
                    </div>
                    <div class="front-news-date  upper-font">28 დეკ. 2015</div>
                    <h2><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                    <p>გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                </div>
                <div class="col-md-4 current-offers-item">
                    <div class="fb-page" data-href="https://www.facebook.com/SPAR.Georgia" data-tabs="timeline" data-height="210" data-small-header="false"  
                        data-show-posts="false"  data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <div class="fb-xfbml-parse-ignore">
                            <blockquote cite="https://www.facebook.com/SPAR.Georgia"><a href="https://www.facebook.com/SPAR.Georgia">SPAR Georgia</a></blockquote>
                        </div>
                    </div>
                    <div class="facebook-follow-title">შემოგვიერთდით სპარის facebook-ზე</div>
                    <div class="facebook-follow-text">პირველებმა მიიღეთ ინფორმაცია სიახლეების და ფასდაკლებების შესახებ!</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-map">
    <div class="home-map-head">
    <div class="container">
        <div class="row">
            <h3 class="section-title upper-font col-md-12">ჩვენი მაღაზიები</h3>
            <div class="tabset1">
                <!-- One Tab  -->
                <div data-pws-tab="tab1" data-pws-tab-name='თბილისი' >
                    <!-- Slider  -->
                    <div class="home-contact-slider-block">
                        <div id="home-contact-slider" class=" clearfix">
                            <div class="our-products-items home-contact-slider">
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                            </div>
                        </div>
                        <!-- End Slider  -->
                    </div>
                    <!-- One Tab  -->       
                </div>
                <div data-pws-tab="tab2" data-pws-tab-name='ქუთაისი' >
                    <!-- Slider  -->
                    <div class="home-contact-slider-block">
                        <div id="home-contact-slider" class=" clearfix">
                            <div class="our-products-items home-contact-slider">
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                                <div class="home-contact-item">
                                    <img src="images/main-logo.png" alt="" />
                                    ი. გოგებაშვილის #54
                                </div>
                            </div>
                        </div>
                        <!-- End Slider  -->
                    </div>
                    <!-- One Tab  -->       
                </div>
            </div>
        </div>
    </div>
</section>
<div class="contact-map map clearfix">
    <div id="googlemaps"></div>
</div>
</section>