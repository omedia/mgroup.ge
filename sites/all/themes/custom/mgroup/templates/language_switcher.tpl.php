<ul class="langs">
	<?php foreach ($links as $link): ?>
		<li>
			<a href="<?php print url($link['href'], array('language' => $link['language'])) ?>">
				<?php print strtoupper(substr($link['language']->name, 0, 2)); ?>
				<img src="<?php print '/'.$image; ?>"/>
			</a>
		</li>
	<?php endforeach; ?>
</ul>