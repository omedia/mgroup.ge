<header class="header">
    <div class="container clearfix">
        <div class="logo">
            <a href="<?php print $front_page;?>">
                <img src="<?php print $logo; ?>" alt="M-Group"/>
            </a>
        </div>
        <?php print $menu_main; ?>
    </div>
</header>