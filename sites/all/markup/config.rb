# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "js"
fonts_dir = "css/fonts"
output_style = :nested # or :nested or :compact or :compressed
relative_assets = true
line_comments = false
preferred_syntax = :scss