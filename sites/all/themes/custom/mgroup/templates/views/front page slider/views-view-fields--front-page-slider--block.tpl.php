<?php
    global $base_url;
    $counter = $fields['counter']->content;
    $url = $base_url . $fields['path']->content;
?>
<div class="<?php print transliteration_clean_filename($fields['title']->content); ?> section<?php if ($counter == 1) print ' active'; ?>"
     id="section<?php print $counter; ?>"
     data-color="<?php print $fields['field_section_color']->content; ?>"
     data-url="<?php print $url; ?> ">
    <div class="container-img" style="background:url(<?php print $fields['field_image']->content; ?>)">
        <div class="emblema" style="background:url(<?php print $fields['field_emblem']->content; ?>)"></div>
        <div class="section-meniu-open-ic mobile-only"></div>
        <div class="section-menu-block">
            <nav class="section-menu" data-color="<?php print $fields['field_menu_items_color']->content; ?>">
                <ul>
                    <li>
                        <a href="<?php print $url;?>#about"><?php print t('ABOUT RESTRAUNT'); ?></a>
                    </li>
                    <li>
                        <a href="<?php print $url;?>#gallery"><?php print t('PHOTO & VIDEO GALLERY'); ?></a>
                    </li>
                    <li>
                        <a href="<?php print $url;?>#calendar"><?php print t('EVENTS CALENDAR'); ?></a>
                    </li>
                    <li>
                        <a href="<?php print $url;?>#location"><?php print t('LOCATION'); ?></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>