<div class="site-content basic-page">
    <div class="container">

        <h3 class="page-title"><?php print $title; ?></h3>

        <div class="page-content-wrap">

            <div class="page-content basic-page-content clearfix">
                <?php
                    print render($content);
                ?>
            </div>
        </div>
    </div>
</div>