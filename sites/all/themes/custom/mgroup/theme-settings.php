<?php
function mgroup_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['theme_settings']['render_submenus'] = array(
    '#type' => 'checkbox',
    '#title' => t('Render submenus in menu'),
    '#default_value' => theme_get_setting('render_submenus'),
  );

  $form['site_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Info'),
    '#weight' => '0'
  );

  $form['site_info']['address'] = array(
      '#type' => 'textfield',
      '#title' => t('Address'),
      '#default_value' => theme_get_setting('address'),
  );

  $form['site_info']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#default_value' => theme_get_setting('email'),
  );

  $form['site_info']['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#default_value' => theme_get_setting('phone'),
  );

  $form['site_info']['location'] = array(
      '#type' => 'fieldset',
      '#title' => t('Position on contact map'),
  );

  $form['site_info']['location']['lat'] = array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#default_value' => theme_get_setting('lat'),
  );

  $form['site_info']['location']['lng'] = array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#default_value' => theme_get_setting('lng'),
  );
}