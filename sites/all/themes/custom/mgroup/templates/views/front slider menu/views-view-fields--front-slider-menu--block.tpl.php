<li data-menuanchor="<?php print transliteration_clean_filename($fields['title']->content); ?>"
    data-color="<?php print $fields['field_loop_menu_color']->content; ?>"
    class="<?php if ($fields['counter']->content == 1) print('active');?>">
    <a href="#<?php print transliteration_clean_filename($fields['title']->content); ?>">
        <?php print $fields['title']->content; ?>
    </a>
</li>