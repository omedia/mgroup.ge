<div class="site-content contact-page">
    <div class="container">
        <h3 class="page-title"><?php print t('Contact'); ?></h3>
        <div class="page-content-wrap">
            <div class="page-content clearfix">
                <div class="contact-left">
                    <div id="googlemaps"></div>
                </div>
                <div class="contact-right">
                    <h3 class="contact-title"><?php print t('Contact Information'); ?></h3>
                    <div class="contact-information">
                        <p class="contact-information-item contact-address"><?php print variable_get('sa_address'); ?></p>
                        <p class="contact-information-item  contact-phone"><?php print variable_get('sa_email'); ?></p>
                        <p class="contact-information-item  contact-mail"><?php print variable_get('sa_phone'); ?></p>
                    </div>
                    <div class="contact-form">
                        <h3 class="contact-title"><?php print t('Write Us'); ?></h3>
                        <?php print render($form); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
