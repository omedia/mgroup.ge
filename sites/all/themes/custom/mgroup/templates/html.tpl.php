<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" dir="ltr" lang="<?php print $language->language; ?>">
<![endif]-->

<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" dir="ltr" lang="<?php print $language->language; ?>">
<![endif]-->

<!--[if IE 8]>
<html class="no-js lt-ie9" dir="ltr" lang="<?php print $language->language; ?>">
<![endif]-->

<!--[if gt IE 8]>-->
<html class="no-js" lang="<?php print $language->language; ?>">
<!--<![endif]-->

<head profile="<?php print $grddl_profile; ?>">
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
    <?php print $scripts; ?>
    <script>
        var activeCallback = $.Callbacks();
        WebFontConfig = {
            custom: {
                families: ['DJV', 'DJVB', 'DJVM', 'DJVMB']
            },
            active: function () {
                activeCallback.fire();
            }
        };
    </script>
    <script src='https://www.google.com/recaptcha/api.js?hl=<?php print $language->language;?>'></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwIg1fBq1Gq8_QS90kyFRCnXxpoLAJAdU" async defer></script>
</head>


<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
</body>
</html>
