<ul class="langs">
	<?php foreach ($links as $link): ?>
		<li><a href="<?php print url($link['href'], array('language' => $link['language'])) ?>"><?php print substr($link['language']->name, 0, 3); ?></a></li>
	<?php endforeach; ?>
</ul>