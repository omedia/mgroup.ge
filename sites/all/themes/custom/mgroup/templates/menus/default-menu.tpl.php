


<div class="right header-right">
 <div class="lang-selector mobile-only"><?php print get_language_switcher(); ?></div>
 
 <?php if (!empty($menu)): ?>
    <div class="main-menu-block">
    <div class="mobile-menu-ic mobile-only"></div>
 
        <nav class="main-menu close">
        
           <div class="mobile-menu-close mobile-only"></div>
            <ul class="main-menu-ul">
                <?php foreach ($menu as $key => $item): ?>
                    <li <?php if ($item['is_active']) print 'class="active"'; ?>>
                        <a href="<?php print $item['url'] ?>"><?php print $item['title']; ?></a>
                        <?php if (!empty($item['below'])) {
                            print $item['below'];
                        } ?>
                    </li>
                <?php endforeach; ?>
                   
            </ul>              
            <ul class="add-menu mobile-only">
                <li><a href="">Catering</a></li>
                 <li><a href="">Delivery</a></li> 
                 
                  <li><a href="">Delivery</a></li> 
                   <li><a href="">Delivery</a></li> 
            </ul>
      
        </nav>
        
        

    </div>
<?php endif; ?>

 <div class="lang-selector desctop-only"><?php print get_language_switcher(); ?></div>

</div>