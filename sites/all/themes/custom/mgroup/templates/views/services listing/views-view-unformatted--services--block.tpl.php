<?php
$count = count($rows);
?>

<div class="services-items-block">
    <h3><?php print t('WHAT WE OFFER'); ?></h3>
        <?php foreach($rows as $key => $row) :?>

        <?php if($key % 4 == 0) : ?>
            <div class="services-items clearfix">
        <?php endif; ?>

            <?php print $row; ?>

        <?php if($key % 4 == 3 || $count % 4 != 0 && $key == $count - 1) :?>
            </div>
        <?php endif; ?>

        <?php endforeach; ?>
</div>