<?php

/*
 * implements hook_preprocess_html()
 */
function mgroup_preprocess_html(&$vars)
{
    global $base_url, $language;

    /* remove fuckin nodes (promoted to front page) from homepage */
    if($vars['is_front'])
    {
        unset($vars['page']['content']['system_main']['nodes']);
        unset($vars['page']['content']['system_main']['pager']);
    } else
    {
        $vars['classes_array'][] = 'basic';
    }

    /* theme_path */
    $vars['theme_path'] = $base_url . '/' . path_to_theme() . '/';
    drupal_add_js(array('theme_path' => $vars['theme_path']), 'setting');

    /* base url */
    $vars['base_url'] = $base_url;

    // /* language */
    // drupal_add_js(array('language' => $language), 'setting');
    $uri = request_uri();
    $uri = explode('/', $uri);
    $uri = $uri[count($uri) - 1];
    $vars['classes_array'][] = $uri;

    $unit_classes = array(
        'bread-house'       => 'breadhouse',
        'puris-saxli'       => 'breadhouse',
        'metexis-chrdilshi' => 'dzveli-tbilisi',
        'shadow-metekhi'    => 'dzveli-tbilisi',
        'qvarlis-tba'       => 'kvareli-like',
        'kvareli-lake'      => 'kvareli-like',
        'otiumi'            => 'otium',
        'otium'             => 'otium',
        'maspinzelo'        => 'maspindzelo',
        'maspindzelo'       => 'maspindzelo'
    );

    if(array_key_exists($uri, $unit_classes))
    {
        $vars['classes_array'][] = $unit_classes[$uri];
        $vars['classes_array'][] = 'services'; // front slider tipze ro gaistilos servicebi
    }

}


/*
 * implements hook_css_alter()
 */
function mgroup_css_alter(&$css)
{
    unset($css[drupal_get_path('module', 'system') . '/system.base.css']);
    unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);
    unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
    //unset($css[drupal_get_path('module','system').'/system.messages.css']);
    unset($css[drupal_get_path('module', 'calendar') . '/css/calendar_multiday.css']);
    unset($css[drupal_get_path('module', 'date') . '/date_views/css/date_views.css']);
    unset($css[drupal_get_path('module', 'comment') . '/comment.css']);
    unset($css[drupal_get_path('module', 'date_api') . '/date.css']);
    unset($css[drupal_get_path('module', 'date_popup') . '/themes/datepicker.1.7.css']);
    unset($css[drupal_get_path('module', 'field') . '/theme/field.css']);
    unset($css[drupal_get_path('module', 'node') . '/node.css']);
    unset($css[drupal_get_path('module', 'search') . '/search.css']);
    unset($css[drupal_get_path('module', 'user') . '/user.css']);
    unset($css[drupal_get_path('module', 'views') . '/css/views.css']);
    unset($css[drupal_get_path('module', 'ctools') . '/css/ctools.css']);
}

/*
 * implements hook_js_alter()
 */
function mgroup_js_alter(&$js)
{
}

/*
 * implements hook_preprocess_page()
 */
function mgroup_preprocess_page(&$vars)
{
    global $base_url;

    /* remove fuckin nodes (promoted to front page) from homepage */
    if($vars['is_front'])
    {
        unset($vars['page']['content']['system_main']['nodes']);
        unset($vars['page']['content']['system_main']['pager']);
    }



    /* theme_path */
    $vars['theme_path'] = $base_url . '/' . path_to_theme() . '/';
    drupal_add_js(array('theme_path' => $vars['theme_path']), 'setting');

    /* base url */
    $vars['base_url'] = $base_url;

    /* main menu */
    $vars['menu_main'] = get_menu('main-menu', 0, 'main-menu');

    /* footer menu */
//  $vars['menu_footer'] = get_menu('menu-footer', '', 0, 0);
    $vars['menu_footer'] = get_menu('main-menu', 0, 'menu-footer');

    // /* language switcher */
    //$vars['lang_switcher'] = get_language_switcher();

    /* user-login */
    if(!user_is_logged_in() && current_path() == 'user')
    {
        drupal_goto('user/login');
    }
    if(current_path() == 'user/login')
    {
        drupal_add_css(path_to_theme() . '/css/omedia_login/user-login.css');
        drupal_add_js(path_to_theme() . '/js/omedia_login/user-login.js');
    }

}

/*
 * main menu generation
 * 
 * $menu: drupal's menu name OR array containing menu objects
 * $level: which level (in case of submenus) to render (for recursion)
 * $render_submenus: explicitly define whether render or not submenus
 */
function get_menu($menu, $level = 0, $tplfile = '', $render_submenus = NULL)
{
    global $language;

    if(!is_array($menu))
    {
        $menu = menu_tree_all_data($menu);
    }

    $menu_for_theme = array();

    foreach ($menu as $key => $item)
    {

        if(($item['link']['language'] != $language->language) && $item['link']['language'] != 'und')
        {
            continue;
        }

        $below = NULL;
        if($render_submenus === NULL)
        {
            $render_submenus = theme_get_setting('render_submenus');
        }
        if(!empty($item['below']) && $render_submenus)
        {
            $below = get_menu($item['below'], $level + 1, 'main-menu');
        }

        $item = $item['link'];
        $title = $item['link_title'];
        if($item['link_path'] == '')
        {
            $item['link_path'] = '<front>';
        }

        if(!empty($item['options']['fragment']))
        {
            $url = url($item['link_path'], array('fragment' => $item['options']['fragment']));
        } else
        {
            $url = url($item['link_path']);
        }

        $active_trail = menu_get_active_trail();
        $is_active = ($item['in_active_trail'] == 1)
            || (current_path() == $url)
            || (($item['link_path'] == $active_trail[0]['href']) && (current_path() == 'node'))
            || (strstr(drupal_get_path_alias(current_path()), drupal_get_path_alias($item['link_path'])));


        $class = 'level_' . $level;
        if($level == 0)
        {
            $class = 'root';
        }

        $menu_for_theme[] = array('url'       => $url,
                                  'title'     => $title,
                                  'is_active' => $is_active,
                                  'below'     => $below);
    }

    return theme('get_menu', array('menu'    => $menu_for_theme,
                                   'class'   => $class,
                                   'tplfile' => $tplfile));
}


/*
 * drupal language links
 */
function get_language_links()
{
    $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
    $languages = language_list('enabled');
    $links = array();
    foreach ($languages[1] as $language)
    {
        $links[$language->language] = array(
            'href'       => $path,
            'title'      => $language->native,
            'language'   => $language,
            'attributes' => array('class' => 'language-link'),
        );
    }
    drupal_alter('translation_link', $links, $path);
    return $links;
}


/*
 * implements hook_theme()
 */
function mgroup_theme($existing, $type, $theme, $path)
{
    return array(
        'get_menu' => array(
            'variables' => array(
                'menu'    => array(),
                'class'   => NULL,
                'tplfile' => NULL,
            ),
            'template'  => 'templates/menus/menu'
        ),

        'get_language_links' => array(
            'variables' => array(
                'links' => array(),
            ),
            'template'  => 'templates/language_switcher'
        ),
    );
}

/*
 * implements get_language_switche
 */
function get_language_switcher()
{
    global $language;
    if(drupal_multilingual())
    {
        $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
        $links = language_negotiation_get_switch_links('language', $path);
        $image = variable_get('file_public_path', conf_path() . '/files') . '/lang-ic.jpg';
        unset($links->links[$language->language]);
        if(isset($links->links))
        {
            $output = theme('get_language_links', array('links' => $links->links, 'image' => $image));
            return $output;
        }
    }
    return;
}

function mgroup_preprocess_node(&$vars)
{
    global $language, $base_url;
    $vars['lang_prefix'] = $language->prefix;
    $node = $vars['node'];
    $node_id = $node->nid;
    $node_type = $node->type;

    if($node_type == 'service' || $node_type == 'event')
    {
        $vars['next'] = node_next_prev($node_id, $node_type, 'next');
        $vars['prev'] = node_next_prev($node_id, $node_type, 'prev');
    }

    if($node_type == 'unit' || $node_type == 'front_slider')
    {
        $calendar_array = events_calendar($node_id);
        $position_array = unit_position($node);

        //var events = Drupal.settings.events_calendar;
        drupal_add_js(array('events_calendar' => $calendar_array), 'setting');

        //var events = Drupal.settings.unit_positions;
        drupal_add_js(array('unit_positions' => $position_array), 'setting');

        $vars['next'] = node_next_prev($node_id, $node_type, 'next');
        $vars['prev'] = node_next_prev($node_id, $node_type, 'prev');
    }
}

function mgroup_preprocess_views_view(&$vars)
{
    global $language;
    $view = $vars['view'];
    if($view->name == 'events') {
        $query = new EntityFieldQuery();

        $result = $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'front_slider')
            ->propertyCondition('language', $language->language, '=')
            ->execute();

        $keys  = (array_keys($result['node']));
        $nodes = node_load_multiple($keys);
        $titles = array();

        foreach($nodes as $node) {
            $titles[] = array(
                'title' => $node->title,
                'nid'   => $node->nid
                );
        }
        $vars['node_titles'] = $titles;
    }
}

function node_next_prev($nid, $type, $direction)
{
    global $language, $base_url;

    $current_node_id = $nid;

    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', $type)
        ->propertyCondition('language', $language->language, '=')
        ->execute();

    $nodes = (array_keys($result['node']));
    $nodes_count = count($nodes);
    $current_index = array_search($current_node_id, $nodes);

    if($direction == 'next')
    {
        $node_id = ($nodes_count > ($current_index + 1)) ? $nodes[$current_index + 1] : $nodes[0];
    } else
    {
        if($direction == 'prev')
        {
            $node_id = ($current_index > 0) ? $nodes[$current_index - 1] : $nodes[$nodes_count - 1];
        }
    }
    $alias = drupal_get_path_alias("node/$node_id");
    $node = node_load($node_id);
    $title = $node->title;

    $url = $base_url . '/' . $language->prefix . '/' . $alias;

    return array(
        'url'   => $url,
        'title' => $title
    );
}

function events_calendar($node_id)
{
    global $language, $base_url;

    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'event')
        ->fieldCondition('field_unit', 'target_id', $node_id, '=')
        ->propertyCondition('language', $language->language, '=')
        ->execute();

    $js_array = array();

    if(array_key_exists("node", $result)) {
        $nodes = node_load_multiple(array_keys($result['node']));
        $lang = $language->prefix;

        foreach ($nodes as $node)
        {
            $title = $node->title;
            $date = field_get_items('node', $node, 'field_date')[0]['value'];
            $url = $base_url . '/' . $lang . '/' . drupal_get_path_alias("node/$node->nid");

            $final = array(
                'title' => $title,
                'date'  => $date,
                'url'   => $url,
            );

            array_push($js_array, $final);
        }

    }

    return $js_array;
}

function unit_position($node)
{
    global $language, $base_url;
    $position = field_get_items('node', $node, 'field_coordinates');
    $logo = field_get_items('node', $node, 'field_emblem');
    $path = file_create_url($logo[0]['uri']);

    $js_array = array();

    if(!empty($position)) {
        foreach ($position as $p)
        {
            $final = array(
                'logo' => image_style_url('map_logo', $logo[0]['uri']),
                'lat'  => floatval($p['lat']),
                'lng'  => floatval($p['lng'])
            );

            array_push($js_array, $final);
        }
    }

    return $js_array;
}

function event_exists($date, $nid)
{
    global $language, $base_url;

    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'event')
        ->fieldCondition('field_date', 'value', $date, '=')
        ->fieldCondition('field_unit', 'target_id', $nid, '=')
        ->propertyCondition('language', $language->language, '=')
        ->execute();

    if(!empty($result['node']))
    {
        $key = array_keys($result['node'])[0];
        $node = node_load($key);
        $date = field_get_items('node', $node, 'field_date');
        $title = $node->title;

        $ret = array(
            'nid'   => $key,
            'title' => $title,
            'date'  => $date[0]['value']
        );

        return $ret;
    } else
    {
        return false;
    }
}

function next_event($nid)
{
    global $language;
    $date = date('Y-m-d');

    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'event')
        ->fieldCondition('field_unit', 'target_id', $nid, '=')
        ->propertyCondition('language', $language->language, '=')
        ->fieldCondition('field_date', 'value', $date, '>=')
        ->fieldOrderBy('field_date', 'value', 'ASC')
        ->range(0, 1)
        ->execute();

    $date_format = false;

    if(!empty($result['node']))
    {
        $key = array_keys($result['node']);
        $node = node_load($key[0]);
        $date = field_get_items('node', $node, 'field_date');
        $date = substr($date[0]['value'], 0, 10);
        $date_format = date('j M', strtotime($date));
    }

    return $date_format;
}

function svg_icon($name, $extra_classes = '')
{
    $url = path_to_theme();
    return '<svg role="image" class="icon-' . $name . ' ' . $extra_classes . '">'.
    '<use xlink:href=/'. $url .'/images/icons.svg#icon-' . $name . '></use></svg>';
}