<?php
    $lang = array(
        'en' => 'en',
        'ka' => 'ge'
    );

    $body = field_get_items('node', $node, 'body');
    $images = field_get_items('node', $node, 'field_photos');
?>
<div class="site-content basic-page">
    <div class="container">
        <h3 class="page-title"><?php print t('SERVICES'); ?></h3>
        <div class="page-content-wrap">
            <div class="page-content basic-page-content clearfix">
                <div class="services-slider-block">
                    <div class="service-details-date">
                    </div>
                    <div class="services-pagination">
                        <a href="<?php print $prev['url']; ?>" class="service-prev">
                            <?php print t('previous'); ?><br/>
                            <?php print t('service'); ?>
                        </a>
                        <a href="<?php print $next['url']; ?>" class="service-next">
                            <?php print t('next'); ?> <br/>
                            <?php print t('service'); ?>
                        </a>
                    </div>
                    <div class="services-slider">
                        <?php if(!empty($images)) :?>
                            <?php foreach($images as $img) :?>
                                <img src="<?php print file_create_url($img['uri'])?>" alt="" />
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="service-desc-block">
                    <h3><?php print $title; ?></h3>
                    <?php print $body[0]['value']; ?>
                    <div class="services-intro-meta clearfix">
                        <div class="services-left">
                            <a href="#" class="service-soc">
<!--                                --><?//xml version="1.0" encoding="UTF-8"?>
                                <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 502.642 502.642" style="enable-background:new 0 0 502.642 502.642;" xml:space="preserve">
                                    <g>
                                        <path style="fill:#010002;" d="M379.247,179.534h-92.043v-44.867c0,0-5.155-42.667,24.677-42.667c33.715,0,60.635,0,60.635,0V0
                                            H269.279c0,0-86.391-0.367-86.391,86.391c0,18.637-0.086,52.568-0.216,93.143h-59.277v74.074h59.125
                                            c-0.345,117.776-0.755,249.035-0.755,249.035h105.438V253.608h69.587L379.247,179.534z"/>
                                    </g>
                                </svg>
                                Share
                            </a>
                            <a href="#" class="service-soc">
<!--                                --><?//xml version="1.0" encoding="iso-8859-1"?>
                                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!--                                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">-->
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="512.002px" height="512.002px" viewBox="0 0 512.002 512.002" style="enable-background:new 0 0 512.002 512.002;"
                                     xml:space="preserve">
                                    <g>
                                        <path d="M512.002,97.211c-18.84,8.354-39.082,14.001-60.33,16.54c21.686-13,38.342-33.585,46.186-58.115
                                            c-20.299,12.039-42.777,20.78-66.705,25.49c-19.16-20.415-46.461-33.17-76.674-33.17c-58.011,0-105.042,47.029-105.042,105.039
                                            c0,8.233,0.929,16.25,2.72,23.939c-87.3-4.382-164.701-46.2-216.509-109.753c-9.042,15.514-14.223,33.558-14.223,52.809
                                            c0,36.444,18.544,68.596,46.73,87.433c-17.219-0.546-33.416-5.271-47.577-13.139c-0.01,0.438-0.01,0.878-0.01,1.321
                                            c0,50.894,36.209,93.348,84.261,103c-8.813,2.399-18.094,3.687-27.674,3.687c-6.769,0-13.349-0.66-19.764-1.888
                                            c13.368,41.73,52.16,72.104,98.126,72.949c-35.95,28.176-81.243,44.967-130.458,44.967c-8.479,0-16.84-0.496-25.058-1.471
                                            c46.486,29.807,101.701,47.197,161.021,47.197c193.211,0,298.868-160.062,298.868-298.872c0-4.554-0.104-9.084-0.305-13.59
                                            C480.111,136.775,497.92,118.275,512.002,97.211z"/>
                                    </g>
                                </svg>
                                Tweet
                            </a>
                        </div>
                        <div class="services-right">
                            <div class="back-link">
                                <a  href="<?php print url('services'); ?>"> <?php print t('back'); ?> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
