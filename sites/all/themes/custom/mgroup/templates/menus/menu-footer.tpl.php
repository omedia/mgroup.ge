<?php if (!empty($menu)): ?>
        <nav class="footer-menu">
            <ul>
                <?php foreach ($menu as $key => $item): ?>
                    <li>
                        <a href="<?php print $item['url'] ?>"><?php print $item['title']; ?></a>
                        <?php if (!empty($item['below'])) {
                            print $item['below'];
                        } ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>
<?php endif; ?>