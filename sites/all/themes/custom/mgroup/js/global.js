(function ($) {
    $(document).ready(function () {

		
				 if ($(window).width() < 769)
				 {

					$(".section").css("height",$(".container-img").width()/1.87);
					$(".main-menu").css("width",$(window).width()/1.3);
					$(".main-menu").css("height",$(window).height());
					$(".main-menu").css("min-height",$(".main-menu-ul").height()+$(".add-menu").height()+170);
					 
					 
					  $(window).resize(function () {
						  $(".section").css("height",$(".container-img").width()/1.87);
						  $(".main-menu").css("width",$(window).width()/1.2);
						  $(".main-menu").css("height",$(window).height());
					  });
					  
					 	   $('#fullpage .section').each(function () { 
						   
						   $(this).find(".section-meniu-open-ic").click(function () {
							   
							   $(this).parent().find(".section-menu-block").toggleClass("open")
							   });
						   
					  });
					  
					  		    $(".mobile-menu-ic,.mobile-menu-close").click(function () {
			$(".main-menu").toggleClass("close");
			$(".mobile-menu-ic").toggleClass("open");
			$(".logo").toggleClass("hide");
				
			
				
				});
					  
					  
				 }
		
				 if ($(window).width() > 768)
				 {
					
				 
				 
        $('#fullpage').fullpage({
            //sectionsColor: ['#f2f2f2', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff']
            //anchors are generated dynamically with content title
            anchors: (function () {
                var anchors = [];
                $('#loop-menu li').each(function () {
                    anchors.push($(this).data('menuanchor'))
                });
                anchors.push('footer-section');
                return anchors;
            })(),
            menu: '#loop-menu',
            loopHorizontal: false,
            continuousVertical: false,
            controlArrows: true,
            paddingTop: '116px',
            fixedElements: '.abs-block'
        });
				

        //style tag is added with generated styles for front page slider section colors
        (function setSectionColors() {
            var fpViews = [];
            var sections = [];
            var loopMenuColors = [];
            var sectionColors = [];
            var sectionMenuColors = [];

            $('#loop-menu li').each(function () {
                sections.push($(this).data('menuanchor'));
                fpViews.push('fp-viewing-' + $(this).data('menuanchor'));
                loopMenuColors.push($(this).data('color'))
            });
            sections.forEach(function (currentValue, index) {
                sectionColors.push($('.' + currentValue + '.section').data('color'));
                sectionMenuColors.push($('.' + currentValue + '.section .section-menu').data('color'));
            });

            var style = document.createElement('style');

            for (var i = 0; i < fpViews.length; i++) {
                style.innerHTML += '.' + fpViews[i] + ' #loop-menu a{color:' + loopMenuColors[i] + '}';
                style.innerHTML += '.' + fpViews[i] + ' #section-menu-ic{background-color:' + sectionColors[i] + '}';
                style.innerHTML += '.' + fpViews[i] + ' #block-up-arrow{background-color:' + sectionColors[i] + '}';
                style.innerHTML += '.' + fpViews[i] + ' #block-down-arrow{background-color:' + sectionColors[i] + '}';
                style.innerHTML += '.' + fpViews[i] + ' .section-menu-block{background-color:' + convertHex(sectionColors[i], 0.95) + '}';
                style.innerHTML += '.' + fpViews[i] + ' .section-menu-block a{color:' + sectionMenuColors[i] + '}';
            }

            function convertHex(hex, opacity) {
                hex = hex.replace('#', '');
                r = parseInt(hex.substring(0, 2), 16);
                g = parseInt(hex.substring(2, 4), 16);
                b = parseInt(hex.substring(4, 6), 16);

                result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity + ')';
                return result;
            }

            $('head').append(style);
        })();

 }

        $('.home-research-tabs').pwstabs({
            effect: 'scale',              // You can change effects of your tabs container: scale / slideleft / slideright / slidetop / slidedown / none
            defaultTab: 1,                // The tab we want to be opened by default
            containerWidth: '100%',     // Set custom container width if not set then 100% is used
            tabsPosition: 'vertical',   // Tabs position: horizontal / vertical
            horizontalPosition: 'top',    // Tabs horizontal position: top / bottom
            verticalPosition: 'left',     // Tabs vertical position: left / right

            theme: '',
            rtl: false                    // Right to left support: true/ false
        });


        $('#loop-menu').css("height", $('#loop-menu ul').height());


        $('.header').css("width", $(window).width() - 129);

        $(window).resize(function () {
            $('.header').css("width", $(window).width() - 129);

        });


        $(".section-menu").each(function () {

            $(this).css("height", $(this).find("ul").height());


        });


        $('#section-menu-ic').on('click', function () {


            $('.active .section-menu-block').toggleClass("open");


            $('body').toggleClass("noactive-menu");


        });


        $('.overflow-layer,.header, #loop-menu').on('click', function () {


            $('.active .section-menu-block').removeClass("open");


            $('body').removeClass("noactive-menu");


        });


        /*navigation front page*/
        $('#block-down-arrow').on('click', function (event) {
            $.fn.fullpage.moveSectionDown();
            $('body').removeClass("noactive-menu");
            $('.section-menu-block').removeClass("open");
        });

        /*navigation front page*/
        $('#block-up-arrow').on('click', function (event) {
            $.fn.fullpage.moveSectionUp();
            $('body').removeClass("noactive-menu");
            $('.section-menu-block').removeClass("open");
        });

        $('.embed-soc .pws_tabs_list').css("min-height", $(".footer-top").height());

        $('.abs-block').css("height", $(".abs-block-in").height());

			 if ($(window).width() > 768)
				 {
        if ($("body").hasClass("main")) {

            function addHandler(object, event, handler) {
                if (object.addEventListener) {
                    object.addEventListener(event, handler, false);
                }
                else if (object.attachEvent) {
                    object.attachEvent('on' + event, handler);
                }
                else alert("Обработчик не поддерживается");
            }

            addHandler(window, 'DOMMouseScroll', wheel);
            addHandler(window, 'mousewheel', wheel);
            addHandler(document, 'mousewheel', wheel);

            function wheel(event) {
                var delta;
                event = event || window.event;

                if (event.wheelDelta) {
                    delta = event.wheelDelta / 120;

                    if (window.opera) delta = -delta;
                }
                else if (event.detail) {
                    delta = -event.detail / 3;
                }


                if (event.preventDefault) event.preventDefault();
                event.returnValue = false;

                $('body').removeClass("noactive-menu");
                $('.section-menu-block').removeClass("open");
            }


        }

				 }
//Services Slider


        $('.services-slider').bxSlider({

            minSlides: 1,
            maxSlides: 1,
            slideMargin: 0,
            controls: 1,
            pagerCustom: '.items-pager'


        });


        $(".items-pager").smoothTouchScroll({continuousScrolling: false});


        /*
         CONTACT MAP
         */

        /**
         * Created by nacarqeq1a on 5/28/14.
         */
            // The latitude and longitude of your business / place
        (function ($) {
            $(document).ready(function (event) {
                //var position        = [41.691644, 44.814201];
                var contact_map     = Drupal.settings.contact_map;
                var unit_positions  = Drupal.settings.unit_positions;
                var positions       = {};

                if(contact_map) {
                    positions = contact_map;
                } else if(unit_positions) {
                    positions = unit_positions;
                }

                function showGoogleMaps() {
                    var center = {};
                    if(positions.length == 0 || contact_map){
                        center = new google.maps.LatLng('41.691644', '44.814201');
                    } else{
                        center = new google.maps.LatLng(positions[0].lat, positions[0].lng);
                    }

                    var styles = [
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {"color": "#98bd69"}
                            ]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {"color": "#85a95c"}
                            ]
                        }
                    ];
                    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

                    var mapOptions = {

                        scrollwheel: false,

                        zoom: 15, // initialize zoom level - the max value is 21
                        streetViewControl: true, // hide the yellow Street View pegman
                        scaleControl: true, // allow users to zoom the Google Map
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        center: center,


                        scaleControl: false,
                        zoomControl: true,
                        mapTypeControl: false,
                        scrollwheel: false,


                        disableDoubleClickZoom: true,
                        mapTypeControlOptions: {
                            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                        }
                    };


                    //console.log(latLng);
                    map = new google.maps.Map(document.getElementById('googlemaps'), mapOptions);

                    //map style
                    map.mapTypes.set('map_style', styledMap);
                    map.setMapTypeId('map_style');


                     //Show the default red marker at the location
                    if(contact_map) {
                        map.setZoom(12);
                    }
                    positions.forEach(function(obj){
                        if(contact_map) {
                            obj.positions.forEach(function(pos){
                                if(pos) {
                                    var latLng = new google.maps.LatLng(pos.lat, pos.lng);
                                    var icon   = obj.logo || 'http://markup.omedialab.com/mgroup/images/map-selector.png';
                                    marker = new google.maps.Marker({
                                        position: latLng,
                                        map: map,
                                        draggable: false,
                                        animation: google.maps.Animation.DROP,
                                        icon: icon
                                    });
                                }
                            });
                        } else {
                                var latLng = new google.maps.LatLng(obj.lat, obj.lng);
                                var icon   = obj.logo || 'http://markup.omedialab.com/mgroup/images/map-selector.png';
                                marker = new google.maps.Marker({
                                    position: latLng,
                                    map: map,
                                    draggable: false,
                                    animation: google.maps.Animation.DROP,
                                    icon: icon
                                });
                        }
                    });

                }

                google.maps.event.addDomListener(window, 'load', showGoogleMaps);

            });

        }(jQuery));


        $(".filter-select").select2();





        $(window).scroll(function () {
            if ($(this).scrollTop() > 5) {
                $('body').addClass('fixed-header');
				

				
            } else {
                $('body').removeClass('fixed-header');
            }
        });


        //Color Darnen Ligten Function

        function ColorLuminance(hex, lum) {
            // validate hex string
            hex = String(hex).replace(/[^0-9a-f]/gi, '');
            if (hex.length < 6) {
                hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
            }
            lum = lum || 0;
            // convert to decimal and change luminosity
            var rgb = "#", c, i;
            for (i = 0; i < 3; i++) {
                c = parseInt(hex.substr(i * 2, 2), 16);
                c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                rgb += ("00" + c).substr(c.length);
            }
            return rgb;
        }


        var brandcolor = $(".services-brand-page").attr("data-color");
        var brandcolorlight = ColorLuminance(brandcolor, +0.3)
        var brandopin = ColorLuminance(brandcolor, +0.8)
        var brandline = ColorLuminance(brandcolor, +0.5)

        var startSlide = function(){
            var slide = $('.events-timeline-items .events-timeline-item.start').parent().index();
            //carieli slaidebis moshoreba
            //$('.events-timeline-items:nth-child(2)').children().length == $('.events-timeline-items:nth-child(2) .empty-date').length
            return slide;
        };

        $('.events-calendar-slider').bxSlider({

            minSlides: 1,
            maxSlides: 1,
            slideMargin: 0,
            pager: 0,
            controls: true,
            startSlide: startSlide()

        });


      

        $(".items-pager a").on('click', function () {
            $(".items-pager a img").css("border-color", "transparent");

            $(this).find("img").css("border-color", brandcolor);


        });

        $(".services-slider-block .bx-controls a").on('click', function () {
            $(".items-pager a img").css("border-color", "transparent");
            $(".items-pager a.active img").css("border-color", brandcolor);

        });

        var imageUrl = $(".bx-controls-direction a").css('background-image');


        $('.bx-controls-direction a.bx-prev').css('background-image', imageUrl, 'important')


        $('.events-timeline-item').on('click', function () {
            $('.events-timeline-item').removeClass("hovered")


            $(this).addClass("hovered")

        });

        //events date filter
        var month = $('.filter-select.month');
        var year  = $('.filter-select.year');
        var unit  = $('.filter-select.unit');

        var query = {
            'date[value][month]': '',
            'date[value][year]' : '',
            'unit' : ''
        };

        var href = window.location.protocol + '//'+window.location.host + window.location.pathname;

        month.on('change', function(){
            query['date[value][month]'] = $(this).val();
            if(query['date[value][month]'] && query['date[value][year]']) {
                window.location = href + '?' + jQuery.param(query);
            }
        });

        year.on('change', function(){
            query['date[value][year]'] = $(this).val();
            if(query['date[value][month]'] && query['date[value][year]']) {
                window.location = href + '?' + jQuery.param(query);
            }
        });

        unit.on('change', function(){
            query['unit'] = $(this).val();
 //           if() {
                window.location = href + '?' + jQuery.param(query);
            //}
        });


	 if ($(window).width() > 768)
				 {
					
				 
        $('.container-img').on('click', function(){
            var url = $(this).parent().parent().attr('data-url');
            window.location = url;
        })
		
				 }

    });


}(jQuery));

