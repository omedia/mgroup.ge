<?php

function contact_page_form($form, &$form_state) {

    $form['full_name'] = array(
        '#type' => 'textfield',
        '#theme_wrappers' => array(),
        '#required' => TRUE,
        '#attributes' => array(
            'class' => array('contact-input', 'input-style'),
            'placeholder' => 'Full Name'
        ),
        '#prefix' => '<div class="contact-form-item">',
        '#suffix' => '</div>'
    );
    $form['email']    = array(
        '#type' => 'textfield',
        '#theme_wrappers' => array(),
        '#required' => TRUE,
        '#attributes' => array(
            'class' => array('contact-input', 'input-style'),
            'placeholder' => 'Email Address'
        ),
        '#prefix' => '<div class="contact-form-item">',
        '#suffix' => '</div>'
    );

    $form['body'] = array(
        '#type' => 'textarea',
        '#theme_wrappers' => array(),
        '#required' => TRUE,
        '#attributes' => array(
            'class' => array('contact-textarea', 'textarea-style'),
            'placeholder' => 'Text'
        ),
        '#prefix' => '<div class="contact-form-item contact-textarea-item">',
        '#suffix' => '</div>'
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'asd',
        '#prefix' => '<div class="contact-submit-item">',
        '#suffix' => '</div>',
        '#attributes' => array(
            'class' => array('contact-submit')
        ),
    );

    $form['security'] = array(
        '#type'   => 'markup',
        '#prefix' => '<div class="contact-form-item captcha-style">',
        '#suffix' => '</div>',
        '#markup' => '<div class="g-recaptcha" data-sitekey="6LeooRwTAAAAADc2SE7hXUAtX-aTHGyUQwIZSGKg"></div>'
    );

    return $form;
}

function contact_page_form_validate($form, &$form_state) {
    $name  = $form_state['values']['full_name'];
    $email = $form_state['values']['email'];
    $body = $form_state['values']['body'];

//    if(!valid_email_address($email)) {
//        form_set_error('email', t('not correct mail'));
//    }
//
//    if (!empty($name)) {
//        form_set_error('full_name', t('name is empty'));
//    }
//
//    if (!empty($body)) {
//        form_set_error('body', t('body text is empty'));
//    }
}


function contact_page_form_submit($form, &$form_state) {
    $name = $form_state['values']['full_name'];
    $from = $form_state['values']['email'];
    $body = $form_state['values']['body'];
    $text = $name . " ". $body . " " . $from;

    $params = array(
        'body'      => $text,
        'subject'   => t('Contact'),
        'headers'   => 'simple',
    );
    $to = "dvaliika@yahoo.com";

    drupal_mail('contact_form', 'send_link', $to, language_default(), $params, 'demo@demo.com', TRUE);
}