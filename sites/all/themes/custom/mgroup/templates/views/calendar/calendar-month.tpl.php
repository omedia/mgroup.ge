<section class="events-calendar">
    <div class="container">
        <h3 class="events-calendar-title"><?php print t('Events calendar'); ?></h3>

        <div class="events-calendar-slider">
            <div class="events-timeline-items clearfix">
                <?php foreach ((array)$rows as $row): ?>
                    <?php print $row['data']; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>