<?php global $language; ?>

<div class="calendarr-page">
    <h2 class="page-title">
        <span>
            <?php print t('Calendar'); ?>
        </span>
    </h2>

    <div class="calendar-page-content clearfix">
        <div class="calendars-days">
            <div class="calendar-row clearfix">
                <?php if ($rows): ?>
                    <?php print $rows; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

