<?php
$date      = substr($item['date'],0, 10);
$date      = strptime($date, '%Y-%m-%d');
$timestamp = mktime(0, 0, 0, $date['tm_mon']+1, $date['tm_mday'], $date['tm_year']+1900);
$date      =  date('j M', $timestamp);
?>

<div class="events-timeline-item">
  <span class="events-time-line-pin">
<!--    <a href="#" class="events-calendar-text clearfix">-->
<!--      <span class="events-calendar-info">ლისის ტბაზე გაიმართება მორიგე შეხვედრა</span>-->
<!--      <span class="events-calenda-readmore">--><?php //print t('Read More'); ?><!--</span>-->
<!--    </a>-->
  </span><?php print $date; ?>
</div>
