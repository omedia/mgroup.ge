<?php
$counter = $fields['counter'];
?>
<?php if($counter->content % 3 == 0) :?>
    <div class="events-items-row clearfix">
<?php endif; ?>

 <div class="events-item">
    <a href="<?php print $fields['path']->content; ?>" class="events-img">
        <img src="<?php print $fields['field_photos']->content; ?>" alt="" />
        <span class="service-details-date"><span><?php print $fields['field_date']->content; ?></span></span>
    </a>
    <div class="events-item-text">
        <div class="events-item-text-in">
            <h2 class="events-item-title"><a href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h2>
            <p class="events-item-desc">
                <?php print $fields['body']->content; ?>
                <a class="events-readmore" href="<?php print $fields['path']->content; ?>">...<?php print t('Read more');?></a>
            </p>
        </div>
    </div>
 </div>

<?php if($counter->content % 3 == 2 || $counter->content == $counter->handler->original_value-1 && $counter->content % 3 == 2) :?>
    </div>
<?php endif; ?>