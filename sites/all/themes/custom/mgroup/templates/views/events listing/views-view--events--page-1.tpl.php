<?php
$year   = Date('Y');
$month  = Date('n');
$years  = array($year - 1, $year, $year + 1);
$months = array(
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
);
if($month > 6){
    $a      = array_slice($months, $month - 1);
    $b      = array_slice($months, 0, $month - 1);
    $months = array_merge($a, $b);
    $months = array_slice($months, 0, 4);
    $start  = $month - 1;
} else {
    $months = array_slice($months, $month - 1, 4);
    $start  = $month - 1;
};

?>

<div class="site-content events-page">
    <div class="container">
        <h3 class="page-title"><?php print t('EVENTS'); ?></h3>

        <div class="page-content-wrap">
            <div class="page-content  clearfix">
                <div class="filter-items">
                    <div class="filter-select-item">
                        <select class="filter-select month">
                            <option><?php print t('select month') ?></option>
                            <?php foreach ($months as $key => $m) : ?>
                                <option value="<?php print (($key+$start)%12)+1; ?>"> <?php print t($m); ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="filter-select-item">
                        <select class="filter-select year">
                            <option><?php print t('select year') ?></option>
                            <?php foreach ($years as $y) : ?>
                                <option> <?php print $y; ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="filter-select-item">
                        <select class="filter-select unit">
                            <option value="0"><?php print t('select unit') ?></option>
                            <?php foreach ($node_titles as $title) : ?>
                                <option value="<?php print $title['nid']; ?>"> <?php print $title['title']; ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <section class="events-items">
                    <?php if(!empty($rows)) :?>
                        <?php print $rows; ?>
                    <?php else :?>
                        <h1><?php print t('No results'); ?></h1>
                    <?php endif; ?>
                </section>
            </div>
        </div>
    </div>
</div>