<nav id="loop-menu">
    <ul>
        <?php foreach ($rows as $id => $row): ?>
            <?php print $row; ?>
        <?php endforeach; ?>
    </ul>
</nav>

<div class="abs-block">
    <div class="abs-block-in">
        <div id="section-menu-ic"></div>
        <div id="block-up-arrow"></div>
        <div id="block-down-arrow"></div>
    </div>
</div>