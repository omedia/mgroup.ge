<?php
$months = array(
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
);

$counter     = 0;
$node_id     = $view->args[0];
$year        = date("Y"); // current year
$date_now    = date('j M');
$current_day = date('Y-n-j');

$event_today = !empty(event_exists($current_day, $node_id));
$next_event  = next_event($node_id);

?>
<section class="events-calendar" id="calendar">
    <div class="container">
        <h3 class="events-calendar-title"><?php print t('Events calendar'); ?></h3>

        <div class="events-calendar-slider">

            <?php foreach($months as $key => $month): ?>
            <?php
                $days = date('t', mktime(0,0,0, $key, 1, $year));
            ?>
            <div class="events-timeline-items clearfix">
                <?php for($day = 1; $day <= $days; $day++) :?>
                    <?php
                        $date    = $year . '-' . $key . '-' . $day;
                        $event   = event_exists($date, $node_id);
                    ?>
                    <?php if($counter > 0 && $counter % 17 == 0) :?>
                        <div class="events-timeline-items clearfix">
                    <?php endif; ?>

                    <?php if(!empty($event)) :?>
                        <?php
                            $date_format = date('j M', strtotime($event['date']));
                            $nid = $event['nid'];
                            $path = drupal_get_path_alias("node/$nid");
                        ?>
                        <div class="events-timeline-item <?php if($date_format == $next_event) print 'start'?>">
                            <span class="events-time-line-pin">
                               <a href="<?php print $path; ?>" class="events-calendar-text clearfix">
                                   <span class="events-calendar-info"><?php print $event['title']; ?></span>
                                   <span class="events-calenda-readmore"><?php print t('read more'); ?></span>
                               </a>
                            </span> <?php print $date_format; ?>
                        </div>
                        <?php else: ?>
                        <div class="events-timeline-item empty-date">
                            <span class="events-time-line-pin"></span>
                        </div>
                    <?php endif;?>
                        <?php if($counter > 0 && $counter % 17 == 16) :?>
                        </div>
                        <?php endif; ?>
                    <?php $counter++; ?>
                <?php endfor; ?>
            </div>
            <?php endforeach;?>

        </div>
    </div>
</section>