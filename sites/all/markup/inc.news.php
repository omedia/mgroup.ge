<div class="news-page page-wrap page-head-img" style="background:url('images/news-header-bg.jpg');">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-9">
                <h2 class="page-title upper-font"><span>ახალი ამბები</span> </h2>
                <div class="news-items">
                    <article class="news-item clearfix">
                        <div class="news-item-img">
                            <a href="#"><img src="images/news-img.jpg" alt="" /></a>
                        </div>
                        <div class="news-item-text">
                            <div class="news-item-date upper-font"> 28 დეკ. 2015</div>
                            <h2 class="news-item-title"><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                            <p class="news-item-desc">გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                        </div>
                    </article>
                    <article class="news-item clearfix">
                        <div class="news-item-img">
                            <a href="#"><img src="images/news-img.jpg" alt="" /></a>
                        </div>
                        <div class="news-item-text">
                            <div class="news-item-date upper-font"> 28 დეკ. 2015</div>
                            <h2 class="news-item-title"><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                            <p class="news-item-desc">გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                        </div>
                    </article>
                    <article class="news-item clearfix">
                        <div class="news-item-img">
                            <a href="#"><img src="images/news-img.jpg" alt="" /></a>
                        </div>
                        <div class="news-item-text">
                            <div class="news-item-date upper-font"> 28 დეკ. 2015</div>
                            <h2 class="news-item-title"><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                            <p class="news-item-desc">გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                        </div>
                    </article>
                    <article class="news-item clearfix">
                        <div class="news-item-img">
                            <a href="#"><img src="images/news-img.jpg" alt="" /></a>
                        </div>
                        <div class="news-item-text">
                            <div class="news-item-date upper-font"> 28 დეკ. 2015</div>
                            <h2 class="news-item-title"><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                            <p class="news-item-desc">გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                        </div>
                    </article>
                    <article class="news-item clearfix">
                        <div class="news-item-img">
                            <a href="#"><img src="images/news-img.jpg" alt="" /></a>
                        </div>
                        <div class="news-item-text">
                            <div class="news-item-date upper-font"> 28 დეკ. 2015</div>
                            <h2 class="news-item-title"><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                            <p class="news-item-desc">გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                        </div>
                    </article>
                    <article class="news-item clearfix">
                        <div class="news-item-img">
                            <a href="#"><img src="images/news-img.jpg" alt="" /></a>
                        </div>
                        <div class="news-item-text">
                            <div class="news-item-date upper-font"> 28 დეკ. 2015</div>
                            <h2 class="news-item-title"><a href="#">ქუთაისში სპარის პირველი მაღაზია გაიხსნა</a></h2>
                            <p class="news-item-desc">გვეწვიეთ ილია ჭავჭავაძის 55-ში და მიიღეთ სპეციალური ფასდაკლება!</p>
                        </div>
                    </article>
                </div>
            </div>
            <aside class="col-md-3 right-side">
                <div class="right-products right-module">
                    <h2 class="module-title upper-font">ჩვენი პროდუქცია</h2>
                    <div class=" clearfix our-products-right">
                        <div class="our-products-items right-slider">
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-products right-module">
                    <h2 class="module-title upper-font">რჩეული რეცეპტები</h2>
                    <div class=" clearfix our-products-right">
                        <div class="our-products-items right-slider">
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                            <div class="our-products-item">
                                <a href="#">
                                    <img src="images/our-products-img.jpg" alt="" />
                                    <span class="our-products-desc">
                                        <h2 class="">ჭოპორტის მანგო</h2>
                                        <p>ეგზოტიკური ხილი მოყვანილი დალოცვილ ქართულ მიწაზე. კონსერვანტების გარეშე.</p>
                                    </span>
                                </a>
                                <a class="our-products-readmore upper-font">დეტალურად</a>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>
<section class="current-offers section-style" >
    <div class="container">
        <div class="row">
            <h3 class="section-title upper-font col-md-12"> მიმდინარე აქციები</h3>
            <div class="clearfix">
                <div class="col-md-4 current-offers-item">
                    <div class="current-offers-img">
                        <a href="#"> <img src="images/current-offers-img.jpg" alt="" />
                        <span class="discount-price"> <span class="old-price">12.95¢ </span>  <span class="new-price"> 9.95¢</span></span></a>
                    </div>
                    <span class="current-offer-action upper-font">აქცია</span>
                    <h2 class="current-offer-title upper-font"><a href="#">თელიანი ველის ცქრიალა ღვინო საახალწლო ფასად!</a></h2>
                </div>
                <div class="col-md-4 current-offers-item">
                    <div class="current-offers-img">
                        <a href="#"> <img src="images/current-offers-img.jpg" alt="" />
                        <span class="discount-price"> <span class="old-price">12.95¢ </span>  <span class="new-price"> 9.95¢</span></span></a>
                    </div>
                    <span class="current-offer-action upper-font">აქცია</span>
                    <h2 class="current-offer-title upper-font"><a href="#">თელიანი ველის ცქრიალა ღვინო საახალწლო ფასად!</a></h2>
                </div>
                <div class="col-md-4 current-offers-item">
                    <div class="current-offers-img">
                        <a href="#"> <img src="images/current-offers-img.jpg" alt="" />
                        <span class="discount-price"> <span class="old-price">12.95¢ </span>  <span class="new-price"> 9.95¢</span></span></a>
                    </div>
                    <span class="current-offer-action upper-font">აქცია</span>
                    <h2 class="current-offer-title upper-font"><a href="#">თელიანი ველის ცქრიალა ღვინო საახალწლო ფასად!</a></h2>
                </div>
            </div>
            <div class="current-more-offers">
                <a href="#" class="moer-offers upper-font">სხვა აქციები და კომბო-შეთავაზებები</a>
            </div>
        </div>
    </div>
</section>