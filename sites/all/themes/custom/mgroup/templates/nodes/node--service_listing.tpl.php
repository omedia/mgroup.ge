<?php
    $body = field_get_items('node', $node, 'body');
    $services_view = views_get_view('services');
    $view = $services_view->preview('block');
?>
<div class="site-content services-list-page">
    <div class="container">
        <h3 class="page-title"><?php print t('SERVICES'); ?></h3>
        <div class="page-content-wrap">
            <div class="page-content basic-page-content clearfix">
                <?php print $body[0]['value']?>
                <?php print $view; ?>
        </div>
    </div>
</div>